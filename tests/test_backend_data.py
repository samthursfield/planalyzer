# Lesson Planalyzer
# Copyright (C) 2019  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import flask
import pytest

import copy

from planalyze.backend import data, localdatabase


"""Tests for the /data/plans/ API provided by the backend server."""


TEST_ACTIVITIES = [
  {
    "title_full": "Intro: (20 mins)",
    "body": [ "Aim: Test the application", "1. Do some tests", ],
    "title": "Intro",
    "tags": [ "(20 mins)" ],
    "id": None,
  },
  {
    "title_full": "Test: Test (10 mins)",
    "body": [ "Aim: Keep testing.", "Let's test some more.", ],
    "title": "Test: Test",
    "tags": [ "(10 mins)" ],
    "id": None,
  },
]

def create_test_activities(plan_id):
    activities = copy.deepcopy(TEST_ACTIVITIES)
    for i, activity in enumerate(activities):
        activity['id'] = plan_id + '_{}'.format(i)
    return activities


TEST_PLANS = {
  "00000000-0000-0000-0000-000000000000": {
    "id": "00000000-0000-0000-0000-000000000000",
    "url": "file:///temp/test1",
    "date": 1533074400,
    "level": "TEST",
    "headline": "Test plan 1",
    "activities": create_test_activities("00000000-0000-0000-0000-000000000000"),
  },
  "11111111-1111-1111-1111-111111111111": {
    "id": "11111111-1111-1111-1111-111111111111",
    "url": "/temp/test2",
    "date": 1536876000,
    "level": "TEST",
    "headline": "Test plan 2",
    "activities": create_test_activities("11111111-1111-1111-1111-111111111111"),
  },
}

TEST_LOGS = {
  "1533074400": {
    "TEST": {
      "url": "file:///logs/1",
      "text": ["This is example log data"],
    }
  }
}


@pytest.fixture
def database(tmpdir):
    database_filename = str(tmpdir.join('data.sqlite'))
    database = localdatabase.LocalDatabase(database_filename)
    database.prepare()

    with database.connection() as db:
        database.insert_plans(db, TEST_PLANS)
    with database.connection() as db:
        database.insert_logs(db, TEST_LOGS)

    return database


@pytest.fixture
def data_app(database):
    """Returns a Flask application with the /data endpoints available."""
    bp = data.make_blueprint(database)
    app = flask.Flask(__name__)
    app.register_blueprint(bp)
    return app.test_client()


def test_levels(data_app):
    result = data_app.get('/data/levels').json
    assert result == ['TEST']


def test_plans_list(data_app):
    result = data_app.get('/data/plans/list?levels=TEST,TEST2').json

    assert len(result) == 2
    assert result[0]['id'] == "11111111-1111-1111-1111-111111111111"
    assert result[1]['id'] == "00000000-0000-0000-0000-000000000000"
    assert result[1]['log'] != None


def test_plans_search(data_app):
    # Search for "example", which matches the log data of one plan only.
    result = data_app.get('/data/plans/search?terms=example&levels=TEST').json
    assert result[0]['id'] == '00000000-0000-0000-0000-000000000000'
    assert result[0]['activities'][0]['_match'] == False
    assert result[0]['activities'][1]['_match'] == False
    assert len(result) == 1
