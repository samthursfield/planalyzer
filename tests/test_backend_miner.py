# Lesson Planalyzer
# Copyright (C) 2019  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""Tests for the miner (which detects which files have changed)."""


import flask
import pytest

import os
import tempfile
import time
import zipfile

from planalyze.backend import data, localdatabase, localminer

import logging, sys
handler = logging.StreamHandler(stream=sys.stdout)
logging.getLogger().addHandler(handler)
logging.getLogger().setLevel(logging.DEBUG)

@pytest.fixture
def database(tmpdir):
    database_filename = str(tmpdir.join('data.sqlite'))
    database = localdatabase.LocalDatabase(database_filename)
    database.prepare()
    return database


@pytest.fixture
def data_app(database):
    """Returns a Flask application with the /data endpoints available."""
    bp = data.make_blueprint(database)
    app = flask.Flask(__name__)
    app.register_blueprint(bp)
    return app.test_client()


def search_and_replace_docx(filename, replacements):
    input_docx = zipfile.ZipFile(filename)
    output_docx_handle, output_docx_name = tempfile.mkstemp()
    with zipfile.ZipFile(output_docx_name, 'a') as output_docx:
        with tempfile.TemporaryDirectory() as tempdir:
            with open(input_docx.extract("word/document.xml", tempdir)) as temp_xml_file:
                temp_xml = temp_xml_file.read()

        for text_from, text_to in replacements.items():
            temp_xml = temp_xml.replace(text_from, text_to)

        with tempfile.NamedTemporaryFile('w') as temp_xml_file:
            temp_xml_file.write(temp_xml)

            for file in input_docx.filelist:
                if not file.filename == "word/document.xml":
                    output_docx.writestr(file.filename, input_docx.read(file))
            output_docx.write(temp_xml_file.name, "word/document.xml")

    os.rename(output_docx_name, filename)


def test_rescan_after_change(database, data_app, shared_datadir):
    plan = shared_datadir.joinpath('2001-01-01-test_plan.docx')
    log =  shared_datadir.joinpath('test_log.docx')
    level_map = {'EXAMPLE 1': 'EX1'}
    localminer.process_files(database, [plan], [log], level_map)

    result = data_app.get('/data/plans/list?levels=EX1').json
    assert len(result) == 1
    assert len(result[0]['activities']) == 3

    activities = result[0]['activities']
    assert activities[0]['title'] == 'Introduction'

    search_and_replace_docx(plan, replacements={"Introduction" : "Beginning"})

    mtime = time.time() + 10
    os.utime(plan, (mtime, mtime))

    localminer.process_files(database, [plan], [log], level_map)

    result = data_app.get('/data/plans/list?levels=EX1').json
    assert len(result) == 1
    assert len(result[0]['activities']) == 3

    activities = result[0]['activities']
    assert activities[0]['title'] == 'Beginning'
