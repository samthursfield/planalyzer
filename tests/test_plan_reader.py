# Lesson Planalyzer
# Copyright (C) 2019  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from planalyze.reader import plan_reader


def test_activity_header_parse():
    """Specific tests for parsing activity titles."""
    title, tags = plan_reader.clean_activity_title("foo:bar (baz)")
    assert title == "foo:bar"
    assert tags == ["(baz)"]

    title, tags = plan_reader.clean_activity_title("(10 min filler): picture dictation (10 mins):")
    assert title == "picture dictation"
    assert tags == ["(10 min filler)", "(10 mins)"]


def test_parse_plan(shared_datadir):
    """Parse example plan files and check the output is as expected."""

    plan_file_1 = shared_datadir.joinpath('2001-01-01-test_plan.docx')
    plan = plan_reader.scan_file(plan_file_1)
    level = plan_reader.level_from_headline(plan['headline'])
    assert level == 'EXAMPLE 1'
    assert len(plan['activities']) == 3
    assert plan['activities'][0]['title_full'] == 'Introduction:'

    plan_file_2 = shared_datadir.joinpath('2001-01-02-test_plan_2.docx')
    plan = plan_reader.scan_file(plan_file_2)
    level = plan_reader.level_from_headline(plan['headline'])
    assert level == 'TEST'
    assert len(plan['activities']) == 4
    assert plan['activities'][0]['title_full'] == 'Activity 1 (10 mins)'
