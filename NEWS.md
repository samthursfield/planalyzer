## 0.99.2

 * Reworked level filter
    - Quicker to drill down to a single level
    - Hidden by default, so teaching lots of levels doesn't lead to an ugly UI
    - Now works when you are searching too.
 * New 'activities' view, which gives an overview of all activities from a
   given lesson plan.
 * New 📂 button to open the folder that contains a lesson plan.
 * Improvements to lesson plan parsing.
 * Some fixes to allow the app to work as a webapp again.

## 0.99.1

 * Much faster load times, due to only reading plans which have changed on startup.
 * Lower memory usage and ability to handle more plans, as we now use SQLite in the backend.
 * Search results now have the matched terms highlighted in yellow.
 * Updated JavaScript dependencies, including Riot.js 4.0
 * Minor fixes to plan parsing.
 * Improvements to desktop integration (icon, window title, etc.)
 * Some other small fixes too.

## 0.99.0

 * First experimental release.
