# Planalyzer

If you're a teacher you probably write hundreds of lesson plans in a single
year. The planalyzer helps you browse and search all your old lesson plans.

Your lesson plans are probably word processor documents. They'll need to
follow a specific format in order to be understood by the tool. Look at the
`examples/` directory for some working examples.

## Desktop use

On Linux systems you can use the planalyzer as a desktop app. You first need
to install it from a Git checkout. Follow these instructions to install it
inside your home directory:


    ./mirror-deps.sh
    mkdir build
    meson .. --prefix=$HOME/.local
    cd build
    ninja install

Now you can run the app. You need to tell it where to find your plans and logs
using commandline arguments. To display the example plans, run this command in
the top directory of the Git checkout:

    planalyze-gtk --plan ./examples/Plans/*.docx --log ./examples/Log.docx

## Screenshot

![](./docs/screenshot1.png)
