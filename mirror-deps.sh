#!/bin/sh
set -eu

cd deps
WGET="wget --timestamping"
$WGET https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css
$WGET https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css.map
$WGET https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js
$WGET https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js.map
$WGET https://code.jquery.com/jquery-3.4.1.min.js && cp ./jquery-3.4.1.min.js ./jquery.min.js
$WGET https://unpkg.com/riot@4/riot+compiler.min.js
$WGET https://unpkg.com/moment/min/moment.min.js
$WGET https://underscorejs.org/underscore-min.js
