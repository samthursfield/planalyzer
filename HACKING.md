# Developer notes

## The desktop app

Run the app with `--debug` to enable:

  * verbose logs written to stderr/stdout
  * the [WebKit inspector](https://trac.webkit.org/wiki/WebInspector) (use F12 or CTRL+SHIFT+C to open)

## The plan reader

This component parses the input lesson plans.

You can run it on a specific file like this:

    python3 ./planalyze/reader/plan_reader.py ./tests/data/2001-01-02-test_plan_2.docx

The output is JSON. You may want to use a tool like
[jq](https://stedolan.github.io/jq/) to pretty print the output.
