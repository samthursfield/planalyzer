#!/usr/bin/python3
# Lesson Planalyzer
#
# Copyright 2019 Sam Thursfield <sam@afuera.me.uk>
#
# Lesson Planalyzer is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Lesson Planalyzer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Lesson Planalyzer.  If not, see <https://www.gnu.org/licenses/>.


import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import argparse
import logging
import json
import os
import sys
import threading
import wsgiref.simple_server


PACKAGE_DATA_DIR = '@pkgdatadir@'
PYTHON_SITE_PACKAGES_DIR = '@pythonsitepackagesdir@'

sys.path.insert(1, PYTHON_SITE_PACKAGES_DIR)

from planalyze.backend import localdatabase, localserver
from planalyze.desktop import app


def argument_parser():
    parser = argparse.ArgumentParser(
        description="Planalyzer desktop app")
    parser.add_argument('--debug', action='store_true',
                        help="Enable detailed logging to stderr. "
                             "This also enables the WebKit inspector.")
    parser.add_argument('--log', type=str, nargs='+',
                        help="Log to scan, in .docx format")
    parser.add_argument('--plan', type=str, nargs='+', required=True,
                        help="Lesson plan to scan, in .docx format")
    parser.add_argument('--level-map', type=argparse.FileType('r'),
                        help="Rename levels, using the mapping in the given "
                             "JSON file. The file should contain an object of "
                             "from/to pairs, e.g. { \"Beginner\": \"N1\" }")
    return parser


def main():
    args = argument_parser().parse_args()

    if args.debug:
        handler = logging.StreamHandler(stream=sys.stdout)
        logging.getLogger().addHandler(handler)
        logging.getLogger().setLevel(logging.DEBUG)

    if args.level_map:
        level_map = json.load(args.level_map)
    else:
        level_map = {}

    database_file = os.path.join(app.cache_dir(), 'data.sqlite')

    database = localdatabase.LocalDatabase(database_file)
    database.prepare()

    base_path = os.path.join(PACKAGE_DATA_DIR, 'frontend')
    wsgi_app = localserver.server(base_path, database)

    httpd = wsgiref.simple_server.make_server('localhost', 0, wsgi_app)
    base_uri = 'http://localhost:{}'.format(httpd.server_port)

    httpd_thread = threading.Thread(target=httpd.serve_forever, daemon=True)
    httpd_thread.start()
    logging.debug("Starting server on {}".format(base_uri))

    Gtk.init()

    gtk_app = app.GtkApp(base_uri, database, args.plan, args.log, level_map, args.debug)

    logging.debug("Running GtkApplication")
    gtk_app.run()


main()
