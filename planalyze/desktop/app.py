# Lesson Planalyzer
#
# Copyright 2019 Sam Thursfield <sam@afuera.me.uk>
#
# Lesson Planalyzer is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Lesson Planalyzer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Lesson Planalyzer.  If not, see <https://www.gnu.org/licenses/>.


import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gio, GLib, Gtk, WebKit2

import logging
import os
import sys
import threading
import urllib.parse

from planalyze.backend import localminer

log = logging.getLogger('desktop.app')


def cache_dir():
    path = os.path.join(GLib.get_user_cache_dir(), 'planalyze')
    os.makedirs(path, exist_ok=True)
    return path


def load_data(database, plan_files, log_files, level_map, callback):
    localminer.process_files(database, plan_files, log_files, level_map)
    callback()


class GtkApp(Gtk.Application):
    def __init__(self, base_uri, database, plan_files, log_files, level_map={}, debug=False):
        self.application_id = 'com.gitlab.samthursfield.planalyze'

        self._window = None
        self._debug = debug
        self._base_uri = base_uri
        self._database = database

        self._plan_files = plan_files
        self._log_files = log_files
        self._level_map = level_map

        self._activities_view = None
        self._plans_view = None

        Gtk.Application.__init__(self, application_id=self.application_id,
                                 flags=Gio.ApplicationFlags.FLAGS_NONE)

        if self._debug:
            open_webkit_inspector_action = Gio.SimpleAction.new('open-webkit-inspector', None)
            open_webkit_inspector_action.connect('activate', self.do_open_webkit_inspector)
            self.add_action(open_webkit_inspector_action)
            self.set_accels_for_action('app.open-webkit-inspector', ['<Control><Shift>c', 'F12'])

    def quit_cb(self, *args):
        self.quit()

    def do_startup(self):
        '''Read the lesson plans.'''
        Gtk.Application.do_startup(self)

        load_task = threading.Thread(
            target=lambda: load_data(self._database, self._plan_files, self._log_files, self._level_map,
                                     callback=lambda: GLib.idle_add(self.refresh_frontend_data)))
        load_task.start()

    def refresh_frontend_data(self):
        if self._activities_view:
            log.debug("Refreshing activity data in the frontend")
            self._activities_view.run_javascript("load_data();");
        if self._plans_view:
            log.debug("Refreshing plan data in the frontend")
            self._plans_view.run_javascript("load_data();");

    def do_activate(self):
        '''Show the main window.'''
        if not self._window:
            self._activities_view = self.create_webview('activities')
            self._plans_view = self.create_webview('plans')
            self._stack = self.create_stack(self._activities_view, self._plans_view)
            self._window = self.create_window(self._stack)

        self._window.present()

    def create_webview(self, load_page=None):
        view = WebKit2.WebView.new()

        settings = view.get_settings()
        settings.set_property('allow-universal-access-from-file-urls', True)

        if self._debug:
            settings.set_property('enable-developer-extras', True)

        view.connect('context-menu', self.do_context_menu)

        if load_page:
            uri = urllib.parse.urljoin(self._base_uri, load_page)
            log.debug("Loading: %s", uri)
            view.load_uri(uri)

        return view

    def create_stack(self, activities_view, plans_view):
        stack = Gtk.Stack.new()
        stack.add_titled(plans_view, 'plans', "Plans")
        stack.add_titled(activities_view, 'activities', "Activities")
        stack.show_all()
        return stack

    def create_window(self, stack):
        window = Gtk.ApplicationWindow.new(self)
        window.set_size_request(1024, 728)
        window.set_title("Lesson Planalyzer")

        headerbar = Gtk.HeaderBar.new()
        headerbar.set_has_subtitle(False)
        headerbar.set_show_close_button(True)

        switcher = Gtk.StackSwitcher(stack=stack)
        headerbar.set_custom_title(switcher)

        headerbar.show_all()
        window.set_titlebar(headerbar)

        window.add(stack)

        window.connect('destroy', self.quit_cb)

        return window

    def do_context_menu(self, view, menu, event, hit_test_result):
        if hit_test_result.context_is_selection():
            # Show menu with 'Copy' item only
            menu.remove_all()

            copy_item = WebKit2.ContextMenuItem.new_from_stock_action(WebKit2.ContextMenuAction.COPY)
            menu.append(copy_item)

            return False    # False means "show the menu"
        else:
            return True     # True means "don't show the menu"

    def do_open_webkit_inspector(self, action, params):
        log.info("Opening WebKit inspector")
        webview = self._stack.get_visible_child()
        webview.get_inspector().show()

    def run(self):
        '''Run the app until quit(), then raise any unhandled exception.'''
        self._exception = None

        old_hook = sys.excepthook

        def new_hook(etype, evalue, etb):
            self.quit()
            self._exception = evalue

        try:
            sys.excepthook = new_hook
            Gtk.Application.run(self)
        finally:
            sys.excepthook = old_hook

        if self._exception:
            raise self._exception
