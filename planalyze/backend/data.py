# Lesson Planalyzer
# Copyright (C) 2019  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""Data access API.

This module provides a web API to query the lesson plan data, exposed as a
Flask blueprint.

"""


import flask

import json
import logging
import re

log = logging.getLogger('backend.data')


def sql_safe(s):
    '''Strip special characters from a string, leaving only letters and numbers.

    This should be used any time a string is passed to the database, and
    prevents against SQL injection attacks.

    '''
    return re.sub(r'\W+', '', s)


def make_blueprint(database):
    '''Initialize the /data/ API.'''
    log.info("Creating /data/ blueprint")

    bp = flask.Blueprint('data', __name__, url_prefix='/data')

    def json_response(text):
        return flask.Response(
            response=text,
            status=200,
            mimetype='application/json',
        )

    @bp.route('/')
    def ping():
        return 'Hello world'

    all_levels_query = database.sql_program('all_levels.sql')

    @bp.route('/levels')
    def levels():
        """Returns a list of all level codes."""
        return json_response(database.execute_sql_get_json_result(all_levels_query))

    plans_list_query = database.sql_program('plans_list.sql')

    def _levels_sql_where_expression(levels):
        # Return SQL WHERE clause for the 'plans' query to filter by level code
        levels = levels.split(',')
        level_list_sql = ','.join("'{}'".format(sql_safe(l)) for l in levels)
        return 'plans.level_code IN ({})'.format(level_list_sql)

    def _sql_order_expression(order):
        # Return SQL ORDER BY clause for oldest/newest first plans.
        assert order in ['newest_first', 'oldest_first']
        return 'begin_date DESC' if order == 'newest_first' else ' ORDER BY begin_date ASC'

    @bp.route('/plans/list')
    def plans_list():
        """Returns a list of lesson plan objects."""
        order = flask.request.args.get('order', 'newest_first')
        levels = flask.request.args.get('levels', '')

        where_clause = _levels_sql_where_expression(levels)
        order_clause = _sql_order_expression(order)

        plans_query = plans_list_query.format(where_clause=where_clause, order_clause=order_clause)
        return json_response(database.execute_sql_get_json_result(plans_query))

    plans_search_query = database.sql_program('plans_search.sql')

    @bp.route('/plans/search')
    def plans_search():
        order = flask.request.args.get('order', 'newest_first')
        levels = flask.request.args.get('levels', '')
        terms = flask.request.args['terms'].split(',')

        where_clause = _levels_sql_where_expression(levels)
        order_clause = _sql_order_expression(order)

        # We look for prefix matches for each term. This is necessary for
        # type-ahead find to work, for example we want the term "bana" to match
        # against the word "banana".
        fts_prefix_query = ' '.join('"{}"*'.format(sql_safe(term)) for term in terms)
        fts_expression = 'NEAR ({})'.format(fts_prefix_query)

        logging.debug("Search: %s", format(fts_expression))

        plans_query = plans_search_query.format(where_clause=where_clause, order_clause=order_clause)
        return json_response(database.execute_sql_get_json_result(plans_query, dict(fts_expression=fts_expression)))

    activities_list_query = database.sql_program('activities_list.sql')

    @bp.route('/activities/list')
    def activities_list():
        levels = flask.request.args.get('levels', '')

        where_clause = _levels_sql_where_expression(levels)

        query = activities_list_query.format(where_clause=where_clause)
        cursor = database.connection().execute(query)

        result = []
        for title, ids, plans, oldest_plan, newest_plan in cursor:
            # We have to manually fix the order of the plans subquery, because
            # SQLite's GROUP BY clause will not honour the order that we
            # specify in the ORDER BY clause.
            ids = json.loads(ids)
            plans = list(sorted(json.loads(plans), key=lambda p: p['date'], reverse=True))
            result.append({
                'title': title,
                'ids': ids,
                'plans': plans,
                'oldest_plan': oldest_plan,
                'newest_plan': newest_plan,
            })
        return flask.jsonify(result)

    return bp
