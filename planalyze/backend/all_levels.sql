-- Get the list of levels from the plans table.
SELECT json_group_array(levels_subquery.levels)
  FROM (
       SELECT level_code AS levels
         FROM plans
        GROUP BY level_code
        ORDER BY level_code ASC
       ) AS levels_subquery
