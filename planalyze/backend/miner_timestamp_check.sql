WITH input_files_subquery(last_read_timestamp) AS
        (SELECT COALESCE(MAX(input_files.last_read_timestamp), 0) AS last_read_timestamp
           FROM input_files
          WHERE input_files.url == :url)
SELECT CASE
       WHEN input_files_subquery.last_read_timestamp < DATETIME(:last_modified_time, "unixepoch")
       THEN true    /* we do need to scan the file */
       ELSE false   /* don't need to scan the file */
       END
       FROM input_files_subquery
