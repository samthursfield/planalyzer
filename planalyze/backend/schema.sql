-- Planalyzer database schema.
CREATE TABLE plans (id PRIMARY KEY, url, begin_date, level_code, headline);
CREATE TABLE activities (id PRIMARY KEY, plan_id, n, title_full, title, tags, text);
CREATE TABLE logs (plan_id, url, day, level_code, text);
CREATE TABLE input_files (url, last_read_timestamp);
CREATE TABLE schema (major_version, minor_version);

-- Full-text search indexes.
CREATE VIRTUAL TABLE activities_fts USING fts5(plan_id UNINDEXED, title, text, content='activities', prefix='3');
CREATE VIRTUAL TABLE logs_fts USING fts5(plan_id UNINDEXED, text, content='logs', prefix='3');

-- Triggers to keep the full-text search indexes up to date.
CREATE TRIGGER activities_inserted AFTER INSERT ON activities BEGIN
  INSERT INTO activities_fts(plan_id, title, text) VALUES (new.plan_id, new.title, new.text);
END;
CREATE TRIGGER activities_deleted AFTER DELETE ON activities BEGIN
  INSERT INTO activities_fts(activities_fts, plan_id, title, text) VALUES ('delete', old.plan_id, old.title, old.text);
END;
CREATE TRIGGER activities_updated AFTER UPDATE ON activities BEGIN
  INSERT INTO activities_fts(activities_fts, plan_id, title, text) VALUES ('delete', old.plan_id, old.title, old.text);
  INSERT INTO activities_fts(plan_id, title, text) VALUES (new.plan_id, new.title, new.text);
END;

CREATE TRIGGER logs_inserted AFTER INSERT ON logs BEGIN
  INSERT INTO logs_fts(plan_id, text) VALUES (new.plan_id, new.text);
END;
CREATE TRIGGER logs_deleted AFTER DELETE ON logs BEGIN
  INSERT INTO logs_fts(logs_fts, plan_id, text) VALUES ('delete', old.plan_id, old.text);
END;
CREATE TRIGGER logs_updated AFTER UPDATE ON logs BEGIN
  INSERT INTO logs_fts(logs_fts, plan_id, text) VALUES ('delete', old.plan_id, old.text);
  INSERT INTO logs_fts(plan_id, text) VALUES (new.plan_id, new.text);
END;

-- Mark the database schema version.
INSERT INTO schema(major_version, minor_version) VALUES(1, 0);
