WITH
    fts_activities_subquery(plan_id, rank, rowid, title_highlighted, text_highlighted) AS (
        SELECT plan_id, rank, rowid, HIGHLIGHT(activities_fts, 1, "<span class='match'>", "</span>") AS text_highlighted, HIGHLIGHT(activities_fts, 2, "<span class='match'>", "</span>") AS text_highlighted
          FROM activities_fts
         WHERE activities_fts MATCH :fts_expression
    ),
    fts_logs_subquery(plan_id, rank, rowid, text_highlighted) AS (
        SELECT plan_id, rank, rowid, HIGHLIGHT(logs_fts, 1, "<span class='match'>", "</span>") AS text_highlighted
          FROM logs_fts
         WHERE logs_fts MATCH :fts_expression
    ),
    fts_plans_subquery(plan_id, rank) AS (
        SELECT plan_id, rank FROM fts_activities_subquery UNION SELECT plan_id, rank FROM fts_logs_subquery
    ),
    activity_subquery(plan_id, json_list) AS (
        SELECT activities.plan_id AS plan_id,
               JSON_GROUP_ARRAY(
                   JSON_OBJECT(
                       'id', activities.id,
                       'title_full', activities.title_full,
                       'title', COALESCE(fts.title_highlighted, activities.title),
                       'tags', activities.tags,
                       'text', COALESCE(fts.text_highlighted, activities.text),
                       '_match', CASE WHEN fts.rowid IS NULL THEN false ELSE true END,
                       '_rank', fts.rank
                   )
               ) AS json_list
          FROM activities
               LEFT JOIN (SELECT rank, rowid, title_highlighted, text_highlighted
                            FROM fts_activities_subquery) AS fts
                      ON activities.rowid == fts.rowid
      GROUP BY activities.plan_id
      ORDER BY activities.n
    ),
    log_subquery(plan_id, json_object) AS (
        SELECT logs.plan_id AS plan_id,
               JSON_OBJECT(
                   'text', COALESCE(fts.text_highlighted, logs.text),
                   '_match', CASE WHEN fts.rowid IS NULL THEN false ELSE true END,
                   '_rank', fts.rank
               ) AS json_object
          FROM logs
               LEFT JOIN (SELECT rank, rowid, text_highlighted
                            FROM fts_logs_subquery) AS fts
                      ON logs.rowid == fts.rowid
    )
SELECT JSON_GROUP_ARRAY(JSON(plans_subquery.json_objects))
    FROM (
        SELECT JSON_OBJECT (
                   'id', plans.id,
                   'url', plans.url,
                   'date', plans.begin_date,
                   'level', plans.level_code,
                   'headline', plans.headline,
                   'activities', JSON(activity_subquery.json_list),
                   'log', JSON(log_subquery.json_object)
               ) AS json_objects
          FROM plans
               INNER JOIN activity_subquery ON activity_subquery.plan_id == plans.id
               LEFT JOIN log_subquery ON log_subquery.plan_id == plans.id
         WHERE plans.id IN (SELECT plan_id FROM fts_plans_subquery) AND
               {where_clause}
      ORDER BY (SELECT rank FROM fts_plans_subquery),
               {order_clause}
    ) AS plans_subquery
