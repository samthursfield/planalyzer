# Lesson Planalyzer
# Copyright (C) 2019  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""A simple database backend."""


import json
import logging
import os
import sqlite3
import time

log = logging.getLogger('backend.database')


class LocalDatabase:
    def __init__(self, filename):
        self.filename = filename

    @staticmethod
    def sql_program(filename):
        '''Returns a .sql program.'''
        datadir = os.path.dirname(__file__)
        with open(os.path.join(datadir, filename)) as f:
            return f.read()

    def prepare(self):
        '''Open database and create the database schema if needed.

        '''
        log.info("Creating database")
        db = sqlite3.connect(self.filename)

        try:
            row = db.execute('SELECT major_version, minor_version FROM schema').fetchone()
            major_version, minor_version = row
            log.debug("Found existing database with schema version %s.%s", major_version, minor_version)
            if major_version > 1:
                raise RuntimeError("Incompatible database {}: major version "
                                   "{} is not supported".format(self.filename, major_version))
        except sqlite3.OperationalError as e:
            if 'no such table' in e.args[0]:
                db.executescript(self.sql_program('schema.sql'))
            else:
                raise e

    def execute_sql_get_one_result(self, query, *args):
        '''Execute an SQL query that should return a single result.'''
        db = sqlite3.connect(self.filename)
        cursor = db.execute(query, *args)
        results = cursor.fetchall()
        if len(results) == 0:
            raise LookupError()
        else:
            assert len(results) == 1 and len(results[0]) == 1
            return results[0][0]

    def execute_sql_get_json_result(self, query, *args):
        '''Execute an SQL query that should return a single JSON object as a result.'''
        db = sqlite3.connect(self.filename)
        start_time = time.time()
        cursor = db.execute(query, *args)
        results = cursor.fetchall()
        logging.debug("Completed query in %s seconds", time.time() - start_time)
        assert len(results) == 1 and len(results[0]) == 1
        return results[0][0]

    def connection(self):
        return sqlite3.connect(self.filename)

    def insert_plans(self, cursor, plans):
        for plan in plans.values():
            cursor.execute('INSERT INTO plans (id, url, begin_date, level_code, headline) VALUES (?, ?, ?, ?, ?);',
                           (plan['id'], plan['url'], plan['date'], plan['level'], plan['headline']))
            for i, activity in enumerate(plan['activities']):
                text = '<ol><li>{}</ol>'.format('<li>'.join(activity['body']))
                tags = ','.join(activity['tags'])
                cursor.execute('INSERT INTO activities (id, plan_id, n, title_full, title, tags, text) VALUES (?, ?, ?, ?, ?, ?, ?)',
                               (activity['id'], plan['id'], i, activity['title_full'], activity['title'], tags, text))

    def insert_logs(self, cursor, logs):
        for timestamp, day_entries in logs.items():
            for level_code, entry in day_entries.items():
                text = '<ul><li>{}</ul>'.format('<li>'.join(entry['text']))
                try:
                    plan_id = self.execute_sql_get_one_result('SELECT id FROM plans WHERE begin_date == CAST(? AS INTEGER) AND level_code == ? LIMIT 1', (timestamp, level_code))
                    cursor.execute('INSERT INTO logs (plan_id, url, day, level_code, text) VALUES (?, ?, ?, ?, ?)',
                                   (plan_id, entry['url'], timestamp, level_code, text))
                except LookupError:
                    logging.warn("No matching plan found for log entry: %s %s %s", timestamp, level_code, text)

    def get_all_levels(self):
        all_levels_query = self.sql_program('all_levels.sql')
        return json.loads(self.execute_sql_get_json_result(all_levels_query))
