-- Select lesson plans as JSON objects.
-- This query contains Python .format() markers to allow customisation.
WITH
    activity_subquery(plan_id, json_list) AS (
        SELECT activities.plan_id AS plan_id,
               JSON_GROUP_ARRAY(
                   JSON_OBJECT(
                       'id', activities.id,
                       'title_full', activities.title_full,
                       'title', activities.title,
                       'tags', activities.tags,
                       'text', activities.text
                   )
               ) AS json_list
          FROM activities
      GROUP BY activities.plan_id
      ORDER BY activities.n
    ),
    log_subquery(plan_id, json_object) AS (
        SELECT logs.plan_id AS plan_id,
               JSON_OBJECT(
                   'text', logs.text
               ) AS json_object
          FROM logs
    )
SELECT JSON_GROUP_ARRAY(JSON(plans_subquery.json_objects))
    FROM (
        SELECT JSON_OBJECT (
                   'id', plans.id,
                   'url', plans.url,
                   'date', plans.begin_date,
                   'level', plans.level_code,
                   'headline', plans.headline,
                   'activities', JSON(activity_subquery.json_list),
                   'log', JSON(log_subquery.json_object)
               ) AS json_objects
          FROM plans
               INNER JOIN activity_subquery ON activity_subquery.plan_id == plans.id
               LEFT JOIN log_subquery ON log_subquery.plan_id == plans.id
          WHERE {where_clause}
          ORDER BY {order_clause}
    ) AS plans_subquery
