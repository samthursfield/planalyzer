# Lesson Planalyzer
#
# Copyright 2019 Sam Thursfield <sam@afuera.me.uk>
#
# Lesson Planalyzer is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Lesson Planalyzer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Lesson Planalyzer.  If not, see <https://www.gnu.org/licenses/>.


"""A simple miner to scan and monitor files for changes."""


import logging
import os
import pathlib

from planalyze.reader import plan_reader
from planalyze.reader import log_reader

log = logging.getLogger('backend.miner')


def process_files(database, plan_files, log_files, level_map):
    for filename in plan_files:
        last_modified_time = os.stat(filename).st_mtime
        process_plan_file(database, filename, last_modified_time, level_map)

    levels = database.get_all_levels()

    for filename in log_files:
        last_modified_time = os.stat(filename).st_mtime
        process_log_file(database, filename, last_modified_time, levels)


def process_plan_file(database, filename, last_modified_time, level_map):
    url = pathlib.Path(filename).absolute().as_uri()

    timestamp_check_query = database.sql_program('miner_timestamp_check.sql')

    should_read = database.execute_sql_get_one_result(
        timestamp_check_query, dict(last_modified_time=last_modified_time, url=url))

    if should_read:
        log.debug("Reading plan file %s with mtime %s", filename, last_modified_time)
        with database.connection() as conn:
            cursor = conn.cursor()
            cursor.execute('DELETE FROM plans WHERE plans.url == :url', dict(url=url))
            cursor.execute('DELETE FROM activities WHERE activities.plan_id IN (SELECT id FROM plans WHERE plans.url == :url)', dict(url=url))

            plans = {}
            for plan in plan_reader.read_files([filename], level_map or {}):
                plans[plan['id']] = plan
            database.insert_plans(cursor, plans)

            cursor.execute('INSERT INTO input_files(url, last_read_timestamp) VALUES(?, DATETIME("now"))', (url,))
    else:
        import time
        log.debug("Not reading plan file %s with mtime %s (time now %s)", filename, last_modified_time, time.time())


def process_log_file(database, filename, last_modified_time, levels):
    url = pathlib.Path(filename).absolute().as_uri()

    timestamp_check_query = database.sql_program('miner_timestamp_check.sql')
    should_read = database.execute_sql_get_one_result(
        timestamp_check_query, dict(last_modified_time=last_modified_time, url=url))

    if should_read:
        log.debug("Reading log file %s with mtime %s", filename, last_modified_time)
        with database.connection() as conn:
            cursor = conn.cursor()
            cursor.execute('DELETE FROM logs WHERE logs.url == :url', dict(url=url))

            logs = {}
            for timestamp, entries in log_reader.read_files([filename], levels):
                logs[timestamp] = entries
            database.insert_logs(cursor, logs)
            cursor.execute('INSERT INTO input_files(url, last_read_timestamp) VALUES(?, DATETIME("now"))', (url,))
    else:
        log.debug("Not reading log file %s with mtime %s", filename, last_modified_time)
