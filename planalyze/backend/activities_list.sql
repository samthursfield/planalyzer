-- Due to a deficiency of SQL, we are unable to sort the 'plans' list by date -- the
-- 'GROUP BY' statement loses the existing order of rows and doesn't honour subequent
-- ORDER BY clauses either.
--
-- Therefore we don't return JSON here, so we can fix the array ordering manually in Python.
SELECT activities.title AS title,
       JSON_GROUP_ARRAY(activities.id) AS ids,
       JSON_GROUP_ARRAY(
           JSON_OBJECT(
               'id', plans.id,
               'url', plans.url,
               'date', plans.begin_date,
               'level', plans.level_code,
               'headline', plans.headline
           )
       ) AS plans,
       MIN(plans.begin_date) AS oldest_plan_date,
       MAX(plans.begin_date) aS newest_plan_date
       FROM activities
            INNER JOIN plans ON plans.id == activities.plan_id
      WHERE {where_clause}
   GROUP BY activities.title
   ORDER BY title ASC, plans.begin_date DESC    -- The second part of the ORDER BY is ignored, with SQlite 3.26.0
