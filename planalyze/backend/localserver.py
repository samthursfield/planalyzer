# Lesson Planalyzer
# Copyright (C) 2019  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""A simple web server for use by the desktop version of the app.

This can also be used for local testing of the web version of the app.

"""

import flask

from gi.repository import Gio

import argparse
import collections
import json
import logging
import os

from . import data
from . import localdatabase


def cache_dir():
    path = os.path.join(GLib.get_user_cache_dir(), 'planalyze')
    os.makedirs(path, exist_ok=True)
    return path


def server(base_dir, database):
    webapp = flask.Flask(__name__, static_folder='')

    @webapp.route('/')
    def root():
        return flask.redirect(flask.url_for('plans'))

    @webapp.route('/activities')
    def activities():
        return flask.send_from_directory(base_dir, 'activities.html')

    @webapp.route('/plans')
    def plans():
        return flask.send_from_directory(base_dir, 'plans.html')

    @webapp.route('/<path:filename>')
    def file(filename):
        return flask.send_from_directory(base_dir, filename)

    @webapp.route('/desktop/open', methods=['POST'])
    def desktop_open():
        '''Open a local file using the default application.

        This is a helper method for use by the desktop app only. It's necessary
        because WebKit security restrictions prevent the JavaScript code from
        being able to do this.

        '''
        path = flask.request.args['path']
        Gio.AppInfo.launch_default_for_uri(path)
        # This request should only be called asynchronously, but we respond
        # with a redirect just in case.
        return flask.redirect(flask.url_for('root'))

    @webapp.route('/desktop/open_folder', methods=['POST'])
    def desktop_open_folder():
        '''Open the folder that contains a given file.

        This is a helper method for use by the desktop app only. It's necessary
        because WebKit security restrictions prevent the JavaScript code from
        being able to do this.

        '''
        path = flask.request.args['path']
        Gio.AppInfo.launch_default_for_uri(os.path.dirname(path))
        # This request should only be called asynchronously, but we respond
        # with a redirect just in case.
        return flask.redirect(flask.url_for('root'))

    data_api = data.make_blueprint(database)
    webapp.register_blueprint(data_api)

    return webapp
