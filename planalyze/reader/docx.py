# Lesson Planalyzer
# Copyright (C) 2019  Sam Thursfield <sam@afuera.me.uk>
#
# Lesson Planalyzer is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Lesson Planalyzer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Lesson Planalyzer.  If not, see <https://www.gnu.org/licenses/>.


"""
Helper functions for reading Microsoft word .docx files.
"""


import xml.etree.ElementTree
import zipfile


WORD_NAMESPACE = '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}'
PARA = WORD_NAMESPACE + 'p'
PARA_PROPERTIES = WORD_NAMESPACE + 'pPr'
PARA_STYLE = WORD_NAMESPACE + 'pStyle'
NUMBERED_PARAGRAPH = WORD_NAMESPACE + 'numPr'
TEXT = WORD_NAMESPACE + 't'
TAB = WORD_NAMESPACE + 'tab'
TABLE = WORD_NAMESPACE + 'tbl'
ROW = WORD_NAMESPACE + 'tr'
CELL = WORD_NAMESPACE + 'tc'
VAL = WORD_NAMESPACE + 'val'


def load_docx(file):
    '''Returns the XML document contents from a Microsoft Word .docx file.'''
    # In theory, the ZipFile module accepts "file-like" objects. In practice,
    # I get this error when passing in a file handle that we received from
    # argparse:
    #
    #    zipfile.BadZipFile: File is not a zip file
    #
    with zipfile.ZipFile(file) as docx:
        tree = xml.etree.ElementTree.XML(docx.read('word/document.xml'))
    return tree


def all_paragraphs(docx):
    '''Return all paragraph elements from a parsed .docx file.'''
    return docx.iter(PARA)


def has_heading_style(paragraph):
    properties = paragraph.find(PARA_PROPERTIES)
    if properties is None:
        return False
    style = properties.find(PARA_STYLE)
    if style is None:
        return False
    else:
        return style.attrib[VAL].startswith('Heading')


def has_bullets_or_numbers(paragraph):
    '''Returns True if the given paragraph is part of a bullet or numbered list.'''
    properties = paragraph.find(PARA_PROPERTIES)
    if properties is None:
        return False
    if properties.find(NUMBERED_PARAGRAPH) is None:
        return False
    else:
        return True


def get_text(paragraph):
    tags = paragraph.iter()
    text = []
    for t in tags:
        if t.tag == TEXT:
            text.append(t.text)
        elif t.tag == TAB:
            text.append('\t')
    return ''.join(text)
