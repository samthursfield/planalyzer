# Lesson Planalyzer
# Copyright (C) 2019  Sam Thursfield <sam@afuera.me.uk>
#
# Lesson Planalyzer is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Lesson Planalyzer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Lesson Planalyzer.  If not, see <https://www.gnu.org/licenses/>.


"""
Script to parse my lesson plans.
"""


import argparse
import datetime
import glob
import json
import logging
import os
import pathlib
import re
import sys
import uuid

from planalyze.reader import docx

log = logging.getLogger('reader.plan_reader')


def argument_parser():
    parser = argparse.ArgumentParser(
        description="Extract content from lesson plan documents")
    parser.add_argument(metavar='FILE', type=argparse.FileType('r'), nargs='+',
                        dest='files', help="Lesson plan to scan, in .docx format")
    parser.add_argument('--debug', action='store_true',
                        help="Enable debug logging")
    parser.add_argument('--level-map', type=argparse.FileType('r'),
                        help="Rename levels, using the mapping in the given "
                             "JSON file. The file should contain an object of "
                             "from/to pairs, e.g. { \"Beginner\": \"N1\" }")
    return parser



tag_at_start_regexp = re.compile('^\(.*\).*')
tag_at_end_regexp = re.compile('.*\(.*\)$')
def ignore_section(section, title_ignore_patterns):
    '''Callback to decide if a section should be ignored.

    This is called when the section is about to be saved, so all data has
    been read from the file already.

    '''
    title = section['title_full']

    for p in title_ignore_patterns:
        if title.startswith(p):
            log.debug("Ignoring section due to '%s' in title", p)
            return True

    if len(section['body']) == 0:
        if ':' in title and (tag_at_end_regexp.match(title) or tag_at_start_regexp.match(title)):
            log.debug("Allowing activity with no body")
        else:
            log.debug("Ignoring section with no body, title doesn't look like an activity")
            return True
    return False


def clean_activity_title(title):
    '''Parses an activity heading into the title, and the tags.

    The title will be something like "Grammar: 1st Conditional". The tags
    may be a list like ['(optional)', '(10 mins)'].

    '''
    tag_re = r'\([^)]+\)'
    optional_colon_re = r'(?::\s*)?'
    title_re = r'({0})*{1}([^(]+){1}({0})*'.format(tag_re, optional_colon_re)

    log.debug("Parsing title: '%s'", title)
    match = re.match(title_re, title)

    if match is None:
        raise RuntimeError("Unable to parse title: {} using re {}".format(title, title_re))

    groups = match.groups()

    title = groups[1].strip()
    if title.endswith(':'):
        title = title[:-1]

    tags = list(filter(None, [groups[0], groups[2]]))

    return title, tags


def maybe_get_focus_line(text):
    focus = None
    if text.lower().startswith('focus: '):
        focus = text[len('focus: '):]
    elif text.lower().startswith('lesson aim: '):
        focus = text[len('lesson_aim: '):]
    return focus


def scan_file(file):
    '''Parses the contents of a plan file.'''
    ignore_list = ['Materials', 'Homework']

    doctree = docx.load_docx(file)

    class ScanState():
        '''Intermediate state during the scan process.'''
        current_section = None
        current_body = []

    class ScanResult():
        '''The result which is returned to the caller.'''
        headline = None
        focus_line = None
        sections = []

    state = ScanState()
    result = ScanResult()

    paras = list(docx.all_paragraphs(doctree))

    result.headline = docx.get_text(paras[0])

    def start_activity(state, text):
        '''Called when a new activity heading is encountered by the scanner.'''
        state.current_section = { 'title_full': text.strip() }

    def finish_activity(state, result):
        '''Called when a new activity heading is encountered by the scanner.'''
        if state.current_section is not None:
            # First, save what we just parsed.
            focus_line = maybe_get_focus_line(state.current_section['title_full'])
            if focus_line:
                # The 'Focus:' or 'Lesson aim: 'line is treated as a special piece of metadata.
                if state.current_body:
                    log.debug("Ignoring body of focus line: %s", state.current_body)
                result.focus_line = focus_line
            else:
                # Other sections get added to the list of activities / sections.
                state.current_section['body'] = state.current_body
                if ignore_section(state.current_section, ignore_list):
                    log.debug("Ignoring section: %s", state.current_section)
                else:
                    result.sections.append(state.current_section)

        state.current_body = []
        state.current_section = None

    def process_activity_body(state, para):
        '''Called for each bullet point under an activity heading.'''
        if state.current_section is not None:
            state.current_body.append(docx.get_text(para))
        else:
            log.debug("Ignoring: %s", docx.get_text(para))

    # The scanner main loop.
    for para in paras[1:]:
        if docx.has_bullets_or_numbers(para):
            process_activity_body(state, para)
        else:
            finish_activity(state, result)

            text = docx.get_text(para)
            if len(text.strip()) > 0:
                start_activity(state, text)

    # Post processing.
    drop_sections = []
    for s in result.sections:
        s['title'], s['tags'] = clean_activity_title(s['title_full'])

        if len(s['title'].strip()) == 0:
            drop_sections.append(s)
            log.debug("Dropping: %s (empty title)", s['title_full'])

    for s in drop_sections:
        result.sections.remove(s)

    result_dict = {
        'headline': result.headline,
        'activities': result.sections,
    }

    if result.focus_line:
        result_dict['focus'] = result.focus_line

    return result_dict


def date_from_filename(filename):
    # Assume the date is the first thing in the filename, in YYYY-MM-DD format.
    basename = os.path.basename(filename)
    date = datetime.datetime.strptime(basename[:10], '%Y-%m-%d')
    return date


def level_from_headline(headline):
    # Assume the level name is first thing in the header, and is separated
    # from the rest by more than 1 space.
    parts = headline.expandtabs().split(' ')

    if len(parts) == 0:
        raise RuntimeError("Unable to parse level from headline".format(headline))

    level = parts[0]

    if len(parts) > 1 and parts[1].isalnum():
        # If level code is followed by a space and some text, include that in
        # the code. This allows codes like 'EXAMPLE 1' to be parsed.
        level += ' ' + parts[1]

    if len(level.strip()) == 0:
        raise RuntimeError("Unable to parse level from headline".format(headline))

    return level


def plan_id():
    return str(uuid.uuid4())


def activity_id(plan_id, index):
    return '{}_{}'.format(plan_id, index)


def read_files(filenames, level_map):
    '''Main entry point when called as a module.

    Generates a dict() object for each lesson plan.'''

    # We assume that 1 file == 1 lesson plan.
    for filename in filenames:
        url = pathlib.Path(filename).absolute().as_uri()

        log.info("Scanning file: %s", url)

        try:
            plan = scan_file(filename)

            level = level_from_headline(plan['headline'])
        except Exception as e:
            raise RuntimeError("{}: {}".format(filename, e)) from e

        level = level_map.get(level, level)

        plan.update({
            'id': plan_id(),
            'url': url,
            'date': date_from_filename(filename).timestamp(),
            'level': level,
        })

        for i, activity in enumerate(plan['activities']):
            activity['id'] = activity_id(plan['id'], i)

        yield plan


def main():
    '''Main entry point when called as a command-line tool.'''
    args = argument_parser().parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    if args.level_map:
        level_map = json.load(args.level_map)
    else:
        level_map = {}

    plans = {}

    for p in read_files([f.name for f in args.files], level_map):
        plans[p['id']] = p

    json.dump(plans, sys.stdout)


if __name__ == '__main__':
    main()
