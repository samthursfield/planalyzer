# Lesson Planalyzer
# Copyright (C) 2019  Sam Thursfield <sam@afuera.me.uk>
#
# Lesson Planalyzer is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Lesson Planalyzer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Lesson Planalyzer.  If not, see <https://www.gnu.org/licenses/>.


"""
Script to parse my teaching reflection.
"""


import dateparser

import argparse
import collections
import glob
import json
import logging
import pathlib
import re
import sys

from planalyze.reader import docx

log = logging.getLogger('reader.log_reader')


def argument_parser():
    parser = argparse.ArgumentParser(
        description="Extract content from teaching reflections")
    parser.add_argument(metavar='FILE', type=argparse.FileType('r'), nargs='+',
                        dest='file', help="Log to scan, in .docx format")
    parser.add_argument('--debug', action='store_true',
                        help="Enable debug logging")
    parser.add_argument('--levels', type=str, required=True,
                        help="List of level codes that appear in the logs. "
                             "Comma separated, for example: EN1,EN2,FCE")
    return parser


def ignore_start(patterns, text):
    for p in patterns:
        if text.startswith(p):
            return True
    return False


def parse_date_line(text):
    '''Parse a date heading.'''

    # We allow trailing information, which allows for notes like
    # "24th December: Holiday"
    if ':' in text:
        date_text = text[:text.find(':')]
    else:
        date_text = text
    date = dateparser.parse(date_text, languages=['en'])
    if date is None:
        raise RuntimeError("Couldn't parse date: {}".format(date_text))
    return date


def maybe_get_level_and_time(text, levels):
    level = None
    time = None

    for l in levels:
        if text.upper().startswith(l.upper()):
            if ':' in text:
                level = l
                extra_info = text[len(level):text.find(':')]
                time = extra_info.strip()
                break

    return level, time


def scan_file(file, levels):
    '''Scan a single log file.

    Returns a 'days' dict mapping each day (UNIX timestamp) to a log entries
    dict. The log entries dict maps a level code to the log entries (list of
    strings).

    '''
    url = pathlib.Path(file).absolute().as_uri()

    log.info("Scanning file: %s", url)

    doctree = docx.load_docx(file)

    paras = list(docx.all_paragraphs(doctree))

    class ScanState():
        timestamp = None
        level = None
        entries = None

    state = ScanState()

    result = collections.defaultdict(dict)

    def start_day(state, text):
        date = parse_date_line(text)
        state.timestamp = date.timestamp()
        log.debug("Started day {} ({})".format(state.timestamp, date))

    def finish_day(state, result):
        if state.timestamp:
            result[int(state.timestamp)].update(state.entries)
            log.debug("Finished day {}".format(state.timestamp))

        state.timestamp = None
        state.level = None
        state.entries = {}

    for para in paras:
        text = docx.get_text(para)

        if docx.has_heading_style(para) and len(text.strip()) > 0:
            finish_day(state, result)
            start_day(state, text)
        elif text:
            level, time = maybe_get_level_and_time(text, levels)
            if level:
                state.level = level

            if state.level is None:
                log.warning("Ignoring: {}".format(text))
            else:
                if state.level not in state.entries:
                    state.entries[state.level] = {
                        'text': [],
                        'url': url,
                    }
                state.entries[state.level]['text'].append(text)
    finish_day(state, result)

    return result


def read_files(filenames, levels):
    '''Main entry point when called as a module.

    Generates a (timestamp, entries) tuple for each day's log entry.

    '''
    for filename in filenames:
        days_dict = scan_file(filename, levels)
        yield from days_dict.items()


def main():
    '''Main entry point when called from the commandline.'''
    args = argument_parser().parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    levels = args.levels.split(',')

    entries = {}
    for timestamp, entry in read_files([f.name for f in args.file], levels):
        entries[timestamp] = entry

    json.dump(entries, sys.stdout)


if __name__ == '__main__':
    try:
        main()
    except RuntimeError as e:
        sys.stderr.write("ERROR: %s\n" % e)
        sys.exit(1)
